package sample;

import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextArea;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.*;
import java.util.Iterator;


public class Controller {

    @FXML MenuItem sortir;
    @FXML TextArea texto;
    @FXML MenuItem copiar;
    @FXML MenuItem enganxar;
    @FXML AnchorPane ap;


    public void onChoose(ActionEvent actionEvent) {
        //Agafem el Stage del programa
        Stage stage = (Stage) ap.getScene().getWindow();
        //Agafem l'ID del element que crida el ActionEvent
        Button bt = (Button)actionEvent.getSource();
        String opc = bt.getId();
        //Creem un nou FileChooser i un nou File
        FileChooser fileChooser = new FileChooser();
        File archivo = null;

        switch (opc){
            case "open":
                fileChooser.setTitle("Selecciona el fitxer");
                //Seleccionem el fitxer que volem obrir
                archivo = fileChooser.showOpenDialog(stage);

                String content = "";
                //Obtenim el AbsolutePath del fitxer
                String path = archivo.getAbsolutePath();
                //Creem un FileReader i un BufferedReader
                FileReader fr = null;
                BufferedReader entrada = null;
                try {
                    //Assignem els valors al FileReader i al BufferedReader
                    fr = new FileReader(path);
                    entrada = new BufferedReader(fr);

                    //Mentres el BufferedReader està Ready fa el següent...
                    while (entrada.ready()) {
                        //...En un String anem afegint el contingut del fitxer linea per linea
                        content += entrada.readLine() + "\n";
                    }
                } catch (IOException e) {
                    e.printStackTrace();

                }
                //Al TextArea li fiquem el text del arxiu
                texto.setText(content);
                //Al Stage li canviem el títol del Stage per el nom del arxiu
                stage.setTitle(archivo.getName());
                break;
            case "save":
                fileChooser.setTitle("Desa el fitxer");
                //Seleccionem el nom i on es guarda el fitxer
                archivo = fileChooser.showSaveDialog(stage);
                //Agafa els paràgrafs del contigunt del TextArea
                ObservableList<CharSequence> paragraph = texto.getParagraphs();
                //Creem un iterador
                Iterator<CharSequence> iter = paragraph.iterator();
                try
                {
                    //Creem un BufferedWriter que agafa el arxiu seleccionat
                    BufferedWriter bf = new BufferedWriter(new FileWriter(archivo));

                    //Mentres hi han mes paràgrafs fa el següent...
                    while(iter.hasNext())
                    {
                        //...Escriu al final del fitxer el text llegit
                        CharSequence seq = iter.next();
                        bf.append(seq);
                        bf.newLine();
                    }
                    //Tanquem el File
                    bf.flush();
                    bf.close();
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
                break;
        }

    }

    //Tanca el programa.
    public void onSortir(ActionEvent actionEvent){
        Platform.exit();
    }

    public void onStyle (ActionEvent actionEvent){
        //Agafem l'ID del element que genera el ActionEvent.
        MenuItem mi = (MenuItem)actionEvent.getSource();
        String opc = mi.getId();

        //Fam   -> guarda el tipus de font actual que es troba al TextArea.
        //size  -> guarda el tamany de la font actual que es troba al TextArea.
        String Fam = texto.getFont().getFamily();
        double size = texto.getFont().getSize();

        //Segons l'opció cambiarà el tamany de la font o el tipus de font.
        switch (opc) {
            case "s8":
                texto.setStyle("-fx-font: 8 " + Fam + ";");
                break;
            case "s12":
                texto.setStyle("-fx-font: 12 " + Fam + ";");
                break;
            case "s16":
                texto.setStyle("-fx-font: 16 " + Fam + ";");
                break;
            case "fGentium":
                texto.setStyle("-fx-font: " + size + " Gentium;");
                break;
            case "fSansSerif":
                texto.setStyle("-fx-font: " + size + " SansSerif;");
                break;
            case "fCantarell":
                texto.setStyle("-fx-font: " + size + " Cantarell;");
                break;
        }
    }

    public void onEdit(ActionEvent actionEvent){
        String opc = null;
        //Agafem l'ID del element que crida a l'ActionEvent
        if (actionEvent.getSource() instanceof  Button){
            Button bt = (Button)actionEvent.getSource();
            opc = bt.getId();
        }else if (actionEvent.getSource() instanceof  MenuItem){
            MenuItem mi = (MenuItem)actionEvent.getSource();
            opc = mi.getId();
        }
        //Segons l'ID seleccionada realitzarà una opció o altra.
        switch (opc){
            case "copy":
            case "copiar":
            case "copia":
                //Copia el text seleecionat.
                texto.copy();
                break;
            case "cut":
            case "tallar":
            case "talla":
                //Retalla el text seleccionat.
                texto.cut();
                break;
            case "enganxar":
            case "enganxa":
            case "paste":
                //Enganxa el text copiat.
                texto.paste();
                break;
            case "desfer":
            case "desfes":
                //Fa el mateix que el Ctrl + Z
                texto.undo();
        }

    }

    public void onDesplegar (){
        //Si el TextArea està buit les opcions Copiar i Enganxar estaran deshabilitades, si no,
        //estaran habilitats.
        if (texto.getSelectedText().length() == 0){
            copiar.setDisable(true);
            enganxar.setDisable(true);
        } else {
            copiar.setDisable(false);
            enganxar.setDisable(false);
        }


    }

    public void onAjuda (ActionEvent actionEvent){
        //Creem una nova Alert i modifiquem els paràmetres necessaris
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Sobre Maicrosoft Guord");
        alert.setHeaderText(null);
        alert.setContentText("By: Kilian Ginesta\nVersion 1.0\n05/11/2019");
        alert.showAndWait();
    }


}
